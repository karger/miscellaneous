#!/bin/bash

#SBATCH --job-name=CH_conv
#SBATCH -A node # Node Account
#SBATCH --partition=node # normal priority level
#SBATCH --qos=normal # normal priority level
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --mail-type=SUBMIT,END,FAIL
#SBATCH --output=/home/karger/logs/CH_crop_%A_%a.out.out
#SBATCH --error=/home/karger/logs/CH_crop_%A_%a.err
#SBATCH --time=639:00:00
#SBATCH --cpus-per-task=10
#SBATCH --mem-per-cpu=3G
#SBATCH --ntasks=1

FILE=$(sed "${SLURM_ARRAY_TASK_ID}q;d" /storage/karger/chelsa_V2/OUTPUT_DAILY/files4transform)
IN='/storage/karger/chelsa_V2/OUTPUT_DAILY/pr_60MB_[dep]/'${FILE}
OUT='/storage/karger/chelsa_V2/OUTPUT_DAILY/prec/'${FILE}

singularity exec -B /storage /home/karger/singularity/chelsa_V2.1.ismip.cont gdal_calc.py --calc="A*10" -A ${IN} --outfile ${OUT} --co "COMPRESS=DEFLATE" --co "PREDICTOR=2"


