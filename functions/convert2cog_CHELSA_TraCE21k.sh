


month1=1
cent=-100

for cent in $(seq -200 20)
do
for month1 in $(seq 1 12)
do
  echo $month
  echo $cent
month=$(printf "%02d" $month1)
INPUT=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/tasmax/CHELSA_TraCE21k_tasmax_${month1}_${cent}_V1.0.tif
OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/tasmax/CHELSA_TraCE21k_tasmax_${month}_${cent}_V.1.0.tif
if [ -f "$OUTPUT" ]; then
    echo "$OUTPUT exists."
else
gdal_translate -of COG -a_scale 0.1 -a_offset 0.0 -ot UInt16 -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
-co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -co "LEVEL=9" \
-mo activity_id="last_glacial_period" -mo cf_standard_name="air_temperature" -mo experiment_id="last_glacial_period" \
-mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
-mo source_id="CHELSA_TraCE21k" -mo variable="tasmax" -mo variable_long_name="Daily Maximum Near-Surface Air Temperature" \
-mo variable_unit="K" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
-mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
-mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
-mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
fi
done
done

for cent in $(seq -200 20)
do
for month1 in $(seq 1 12)
do
  echo $month
  echo $cent
  month=$(printf "%02d" $month1)
INPUT=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/tasmin/CHELSA_TraCE21k_tasmin_${month1}_${cent}_V1.0.tif
OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/tasmin/CHELSA_TraCE21k_tasmin_${month}_${cent}_V.1.0.tif
if [ -f "$OUTPUT" ]; then
    echo "$OUTPUT exists."
else
gdal_translate -of COG -a_scale 0.1 -a_offset 0.0 -ot UInt16 -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
-co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -co "LEVEL=9" \
-mo activity_id="last_glacial_period" -mo cf_standard_name="air_temperature" -mo experiment_id="last_glacial_period" \
-mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
-mo source_id="CHELSA_TraCE21k" -mo variable="tasmin" -mo variable_long_name="Daily Minimum Near-Surface Air Temperature" \
-mo variable_unit="K" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
-mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
-mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
-mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
fi
done
done

for cent in $(seq -200 20)
do
for month1 in $(seq 1 12)
do
  echo $month
  echo $cent
  month=$(printf "%02d" $month1)
INPUT=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/pr/CHELSA_TraCE21k_pr_${month1}_${cent}_V1.0.tif
OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/pr/CHELSA_TraCE21k_pr_${month}_${cent}_V.1.0.tif
gdal_translate -of COG -a_scale 1.0 -a_offset 0.0 -ot UInt16 -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
-co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -co "LEVEL=9" \
-mo activity_id="last_glacial_period" -mo cf_standard_name="precipitation_flux" -mo experiment_id="last_glacial_period" \
-mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
-mo source_id="CHELSA_TraCE21k" -mo variable="pr" -mo variable_long_name="Precipitation" \
-mo variable_unit="kg m-2 day-1" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
-mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
-mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
-mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
done
done

for cent in $(seq -200 20)
do
for month1 in $(seq 1 12)
do
  echo $month
  echo $cent
  month=$(printf "%02d" $month1)
INPUT=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/tz/CHELSA_TraCE21k_tz_${month1}_${cent}_V1.0.tif
OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/tz/CHELSA_TraCE21k_tz_${month}_${cent}_V.1.0.tif
gdal_translate -of COG -a_scale 1.0 -a_offset 0.0 -ot Float64 -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
-mo activity_id="last_glacial_period" -mo cf_standard_name="air_temperature_lapse_rate" -mo experiment_id="last_glacial_period" \
-mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
-mo source_id="CHELSA_TraCE21k" -mo variable="tz" -mo variable_long_name="Air Temperature Lapse Rate" \
-mo variable_unit="K m-1" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
-mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
-mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
-mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
done
done


for cent in $(seq -200 20)
do
  echo $cent
INPUT=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/orog/CHELSA_TraCE21k_dem_${cent}_V1.0.tif
OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/orog/CHELSA_TraCE21k_orog_${cent}_V.1.0.tif
gdal_translate -of COG -a_scale 1.0 -a_offset 0.0 -ot Int16 -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
-co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -co "LEVEL=9" \
-mo activity_id="last_glacial_period" -mo cf_standard_name="surface_altitude" -mo experiment_id="last_glacial_period" \
-mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
-mo source_id="CHELSA_TraCE21k" -mo variable="orog" -mo variable_long_name="Surface Altitude" \
-mo variable_unit="m" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
-mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
-mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
-mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
done


for cent in $(seq -200 20)
do
  echo $cent
INPUT=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/orog/CHELSA_TraCE21k_glz_${cent}_V1.0.tif
OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/glz/CHELSA_TraCE21k_glz_${cent}_V.1.0.tif
gdal_translate -of COG -a_scale 1.0 -a_offset 0.0 -ot Int16 -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
-co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -co "LEVEL=9" \
-mo activity_id="last_glacial_period" -mo cf_standard_name="ice_sheet_surface_altitude" -mo experiment_id="last_glacial_period" \
-mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
-mo source_id="CHELSA_TraCE21k" -mo variable="glz" -mo variable_long_name="Ice Sheet Surface Altitude" \
-mo variable_unit="m" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
-mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
-mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
-mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
done


for cent in $(seq -200 20)
do
  echo $cent
INPUT=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/orog/CHELSA_TraCE21k_gle_${cent}_V1.0.tif
OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/gle/CHELSA_TraCE21k_gle_${cent}_V.1.0.tif
gdal_translate -of COG -a_scale 1.0 -a_offset 0.0 -ot Byte -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
-mo activity_id="last_glacial_period" -mo cf_standard_name="ice_sheet_extent" -mo experiment_id="last_glacial_period" \
-mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
-mo source_id="CHELSA_TraCE21k" -mo variable="gle" -mo variable_long_name="Ice Sheet Extent" \
-mo variable_unit="m" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
-mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
-mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
-mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
done


for bion in $(seq 1 19)
do
  echo $bion
done


for cent in $(seq -200 20)

cent=-200
for bion in $(seq 1 19)
do
  echo $bion
if [[ $bion == 1 ]]
then
  shortname="bio01"
  cf_standard_name="air_temperature"
  variable_long_name="Mean Annual Temperature"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 2 ]]
then
  shortname="bio02"
  cf_standard_name="air_temperature"
  variable_long_name="Annual Mean Diurnal Range"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 3 ]]
then
  shortname="bio03"
  cf_standard_name="air_temperature"
  variable_long_name="Isothermality"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=Int16
  unit="K"
fi
if [[ $bion == 4 ]]
then
  shortname="bio04"
  cf_standard_name="air_temperature"
  variable_long_name="Temperature Seasonality"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 5 ]]
then
  shortname="bio05"
  cf_standard_name="air_temperature"
  variable_long_name="Maximum Temperature of the Warmest Month"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 6 ]]
then
  shortname="bio06"
  cf_standard_name="air_temperature"
  variable_long_name="Minimum Temperature of the Coldest Month"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 7 ]]
then
  shortname="bio07"
  cf_standard_name="air_temperature"
  variable_long_name=" Annual Temperature Range"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 8 ]]
then
  shortname="bio08"
  cf_standard_name="air_temperature"
  variable_long_name="Mean Temperature of the Wettest Quarter"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 9 ]]
then
  shortname="bio09"
  cf_standard_name="air_temperature"
  variable_long_name="Mean Temperature of the Driest Quarter"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 10 ]]
then
  shortname="bio10"
  cf_standard_name="air_temperature"
  variable_long_name="Mean Temperature of the Warmest Quarter"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 11 ]]
then
  shortname="bio11"
  cf_standard_name="air_temperature"
  variable_long_name="Mean Temperature of the Coldest Quarter"
  equ="(A+273.15)*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="K"
fi
if [[ $bion == 12 ]]
then
  shortname="bio12"
  cf_standard_name="precipitation_flux"
  variable_long_name="Annual Precipitation"
  equ="A*1"
  scale=1.0
  offset=0.0
  type=UInt16
  unit="kg m-2 year-1"
fi
if [[ $bion == 13 ]]
then
  shortname="bio13"
  cf_standard_name="precipitation_flux"
  variable_long_name="Precipitation of the Wettest Month"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="kg m-2 month-1"
fi
if [[ $bion == 14 ]]
then
  shortname="bio14"
  cf_standard_name="precipitation_flux"
  variable_long_name="Precipitation of the Driest Month"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="kg m-2 month-1"
fi
if [[ $bion == 15 ]]
then
  shortname="bio15"
  cf_standard_name="precipitation_flux"
  variable_long_name="Precipitation Seasonality"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="kg m-2 month-1"
fi
if [[ $bion == 16 ]]
then
  shortname="bio16"
  cf_standard_name="precipitation_flux"
  variable_long_name="Precipitation of the Wettest Quarter"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="kg m-2 month-1"
fi
if [[ $bion == 17 ]]
then
  shortname="bio17"
  cf_standard_name="precipitation_flux"
  variable_long_name="Precipitation of the Driest Quarter"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="kg m-2 month-1"
fi
if [[ $bion == 18 ]]
then
  shortname="bio18"
  cf_standard_name="precipitation_flux"
  variable_long_name="Precipitation of the Warmest Quarter"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="kg m-2 month-1"
fi
if [[ $bion == 19 ]]
then
  shortname="bio19"
  cf_standard_name="precipitation_flux"
  variable_long_name="Precipitation of the Coldest Quarter"
  equ="A*10"
  scale=0.1
  offset=0.0
  type=UInt16
  unit="kg m-2 month-1"
fi
  echo $cent
  echo "$shortname"
  echo "$cf_standard_name"
  echo "$variable_long_name"

  INPUT1=/vsicurl/https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V1/chelsa_trace/bio/CHELSA_TraCE21k_bio$(printf "%02d" $bion)_${cent}_V1.0.tif
  OUTPUT=/mnt/chelsa01/chelsa_trace21k/global/centennial/bio$(printf "%02d" $bion)/CHELSA_TraCE21k_bio$(printf "%02d" $bion)_${cent}_V.1.0.tif
  gdal_calc.py -A ${INPUT1} --outfile=/home/karger/scratch/tmp.tif --calc=${equ}
  INPUT=/home/karger/scratch/tmp.tif
  gdal_translate -of COG -a_scale ${scale} -a_offset ${offset} -ot ${type} -co "NUM_THREADS=ALL_CPUS" -mo forcing="CCSM3-TraCE21k" \
  -co "COMPRESS=DEFLATE" -co "PREDICTOR=2" -co "LEVEL=9" \
  -mo activity_id="last_glacial_period" -mo cf_standard_name="${cf_standard_name}" -mo experiment_id="last_glacial_period" \
  -mo frequency="centennial" -mo institution_id="WSL" -mo product="model-output" -mo project="CHELSA" \
  -mo source_id="CHELSA_TraCE21k" -mo variable="${shortname}" -mo variable_long_name="${variable_long_name}" \
  -mo variable_unit="${unit}" -mo frequency="centennial" -mo version="1.0" -mo datetime="${DATE}" \
  -mo citation1="Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023" \
  -mo data_cite="Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211" \
  -mo contact="dirk.karger@wsl.ch" ${INPUT} ${OUTPUT}
  rm /home/karger/scratch/tmp.tif
done


done


