#!/bin/bash

#SBATCH --job-name=CH_DF
#SBATCH -A node # Node Account
#SBATCH --qos=normal # normal priority level
#SBATCH --mail-user=dirk.karger@wsl.ch
#SBATCH --mail-type=SUBMIT,END,FAIL
#SBATCH --output=/home/karger/logs/chdf_%A_%a.out
#SBATCH --error=/home/karger/logs/dhdf_%A_%a.err
#SBATCH --time=100:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=3G
#SBATCH --ntasks=1

mkdir /home/karger/scratch/${SLURM_ARRAY_TASK_ID}
for i in {1..25000} #17000
do
Rscript /home/karger/scripts/miscellaneous/functions/pointbased_df_tas_downs.R ${i} ${SLURM_ARRAY_TASK_ID}
find /home/karger/scratch/${SLURM_ARRAY_TASK_ID}/. -name "*.txt" | xargs -n 1 tail -n +1 >> /nfs/karger/ch_df/CHELSA_${SLURM_ARRAY_TASK_ID}.txt
done
rm -r /home/karger/scratch/${SLURM_ARRAY_TASK_ID}