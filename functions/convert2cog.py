from functions.saga_functions import grid_calculator_simple
import os
import saga_api
def convert2cog(obj, tmp, filename, scale, offset, ot, st, date,
                cf_standard_name, variable, variable_long_name, variable_unit, freq='day', compress='DEFLATE'):
    """
    scale and save CSG_Data_Object as scaled and compressed cloud optimized geotiff (COG) and set metadata

    :param obj: grid object, CSG_Data_Object
    :param tmp: path to the temporary directory, string
    :param filename: filename, string
    :param scale: scale, float
    :param offset: offset, float
    :param ot: output type, string
    :param st: output type tmp grid, integer
    :param date: date, integer
    :param cf_standard_name: CF standard variable name, string
    :param variable: variable short name, string
    :param variable_long_name: variable long name, string
    :param variable_unit: unit, sting
    :param freq: freqency, string, default=day
    :param compress: compression type, string [DEFLATE, LZW, ZSTD, LERC_ZSTD]

    :return: True
    :rtype: bool
    """
    scale_calc = 1/scale
    offset_calc = offset * (-1)
    date = str(int(year)) + '-' + str("%02d" % (int(month),)) + '-' + str("%02d" % (int(day),))
    obj2 = grid_calculator_simple(obj1=obj,
                                  equ='(a+' + str(offset) + ')*' + str(scale_calc),
                                  type=st)
    obj2.Save(tmp + 'tmp_outfile.sgrd')
    if compress == 'DEFLATE':
        cstr = ' -co \"COMPRESS=DEFLATE\" ' + '-co \"PREDICTOR=2\" ' + '-co \"LEVEL=9\" '

    if compress == 'LZW':
        cstr = ' -co \"COMPRESS=LZW\" '

    if compress == 'ZSTD':
        cstr = ' -co \"COMPRESS=ZSTD\" ' + '-co \"PREDICTOR=2\" '

    if compress == 'LERC':
        cstr = ' -co \"COMPRESS=LERC_ZSTD\" -co \"ZSTD_LEVEL=15\" -co \"MAX_Z_ERROR=5\" '

    sysc = ('gdal_translate -of COG -a_scale ' +
              str(scale) + ' -a_offset ' + str(offset_calc) + ' -ot ' + ot +
              cstr +
              '-co \"NUM_THREADS=ALL_CPUS\" ' +
              ' -mo forcing=\"CCSM3-TraCE21k\"' +
              ' -mo activity_id=\"last_glacial_period\"' +
              ' -mo cf_standard_name=\"' + cf_standard_name + '\"' +
              ' -mo experiment_id=\"last_glacial_period\"' +
              ' -mo frequency=\"centennial\"' +
              ' -mo institution_id=\"WSL\"' +
              ' -mo product=\"model-output\"' +
              ' -mo project=\"CHELSA\"' +
              ' -mo source_id=\"CHELSA_TraCE21k\"' +
              ' -mo variable=\"' + variable + '\"' +
              ' -mo variable_long_name=\"' + variable_long_name + '\"' +
              ' -mo variable_unit=\"' + variable_unit + '\"' +
              ' -mo frequency=\"' + freq + '\"' +
              ' -mo version=\"1.o\"' +
              ' -mo datetime=\"' + date + '\"' +
              ' -mo citation1=\"Karger, D.N., Nobis, M.P., Normand, S., Graham, C.H., Zimmermann, N. (2023) CHELSA-TraCE21k – High resolution (1 km) downscaled transient temperature and precipitation data since the Last Glacial Maximum. Climate of the Past, 19, 439-456. https://doi.org/10.5194/cp-19-439-2023\"' +
              ' -mo data_cite=\"Karger, D. N., Nobis, M. P., Normand, S., Graham, C. H., Zimmermann, N. E. (2020) CHELSA-TraCE21k: Downscaled transient temperature and precipitation data since the last glacial maximum. EnviDat. https://www.doi.org/10.16904/envidat.211\"' +
              ' -mo contact=\"dirk.karger@wsl.ch\" ' +
              tmp + 'tmp_outfile.sdat ' + filename)
    print(sysc)
    os.system(sysc)
    saga_api.SG_Get_Data_Manager().Delete(obj2)

    return True